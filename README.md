# My Docs
This repository holds all my findings so far as regards to web  
app development.  

Files are saved inside of folders depicting a  
given area in web development. 

For example, study done under node  
package manager(npm) are saved in a folder called "npm", with the  
file name depicting a subset of study under the given tech.  

#### list of docs
***
- npm
- angular
- angularJS
- bootstrap
- git
- javascript
- javascript modules
- npm commands
- webpack